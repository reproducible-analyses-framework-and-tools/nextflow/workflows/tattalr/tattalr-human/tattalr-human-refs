import subprocess
import os

manual = []

# Set path to location to current directory
path = os.getcwd()
sums_path = path + '/general_sums.txt'

attempt = 0


def check_prior(file):
	prior = str(subprocess.check_output([f'find . -type d -name {file}'], shell=True))
	if file in prior:
		return True
	else:
		return False


def reference_check(i):
	with open(sums_path, 'r') as f:
		if i in f.read():
			print('File successfully validated: ' + str(i).rsplit(' ', 1)[1])
			return True
		else:
			print('File failed validation: ' + str(i).rsplit(' ', 1)[1])
			return False


def download_reference(cmd, alt=None):
        if alt == None:
                file = cmd.rsplit('/', 1)[1]
        else:
                file = alt
        attempt = 0
#        while True:
#                if attempt == 3:
#                        manual.append(f'{file}')
#                        return False
        subprocess.run([f'{cmd}'], shell=True)
#                check = str(subprocess.check_output([f'md5sum {file}'], shell=True)).split("'", 1)[1].rsplit("\\", 1)[0]
#                if reference_check(check):
        return True
#                else:
#                        subprocess.run([f'rm {file}'], shell=True)
#                        attempt += 1


os.chdir(f'{path}/references')

### MHCflurry ###
if not check_prior('mhcflurry'):
	subprocess.run([f'mkdir -p {path}/references/mhcflurry/tmp'], shell=True)
	os.chdir(f'{path}/references/mhcflurry/tmp')

	if download_reference('wget https://github.com/openvax/mhcflurry/releases/download/pre-2.0/models_class1_presentation.20200611.tar.bz2'):
		subprocess.run(['tar xvf *'], shell=True)
		subprocess.run(['mv models/* ../'], shell=True)
		os.chdir('..')
		subprocess.run(['rm -rf tmp'], shell=True)
else:
	print('Found pre-existing directory: MHCflurry')
os.chdir(f'{path}/references')


### Viral Reference ###
if not check_prior('viral'):
	subprocess.run([f'mkdir -p {path}/references/viral'], shell=True)
	os.chdir(f'{path}/references/viral')

	download_reference('wget https://github.com/dmarron/virdetect/raw/master/reference/virus_masked_hg38.fa')
	download_reference('wget https://gitlab.com/landscape-of-effective-neoantigens-software/nextflow/modules/tools/lens/-/wikis/uploads/4dedb99984857905ee96ab1d148d7863/virdetect.cds.gff.gz')
#	download_reference('wget https://gitlab.com/landscape-of-effective-neoantigens-software/nextflow/modules/tools/lens/-/wikis/uploads/ad52c657d06c12d7a3346f15b71390af/virus.cds.fa.gz')
	download_reference('wget https://gitlab.com/landscape-of-effective-neoantigens-software/nextflow/modules/tools/lens/-/wikis/uploads/ffe4829920d891599a0db1161fdb7583/virus.cds.2024f2.fa')
	download_reference('wget https://gitlab.com/landscape-of-effective-neoantigens-software/nextflow/modules/tools/lens/-/wikis/uploads/9e3a49921bd325caa98dcd9211f8cdd9/virus.pep.fa.gz')
	subprocess.run(['gunzip *.gz'], shell=True)
else:
	print('Found pre-existing directory: Viral')
os.chdir(f'{path}/references')

### Antigen.Garnish ###
if not check_prior('antigen.garnish'):
	os.chdir(f'{path}/references')
	download_reference("curl -fsSL --output antigen.garnish-2.3.0.tar.gz https://s3.amazonaws.com/get.rech.io/antigen.garnish-2.3.0.tar.gz")
	subprocess.run(['tar -xvzf antigen.garnish-2.3.0.tar.gz'], shell=True)
	subprocess.run(['chmod -R 700 antigen.garnish'], shell=True)
	subprocess.run(['rm antigen.garnish-2.3.0.tar.gz'], shell=True)
else:
	print('Found pre-existing directory: antigen.garnish')
os.chdir(f'{path}/references')


### BLASTP ###
if 'blastp' not in str(subprocess.check_output([f'find . -maxdepth 1 -type f -name blastp'], shell=True)):
	subprocess.run([f'mkdir -p {path}/references/bin'], shell=True)
	os.chdir(f'{path}/references/bin')

	if download_reference('wget https://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/2.13.0/ncbi-blast-2.13.0+-x64-linux.tar.gz'):
		subprocess.run(['tar xvf *gz'], shell=True)
		subprocess.run(['mv ncbi*/bin/blastp .'], shell=True)
		subprocess.run(['rm -rf ncbi*'], shell=True)
		subprocess.run(['mv blastp ../antigen.garnish'], shell=True)
		os.chdir('..')
		subprocess.run(['rm -rf bin'], shell=True)
else:
	print('Found pre-existing file: antigen.garnish/blastp')
os.chdir(f'{path}/references')

### DUMMY FILE ###
print('\nCreating dummy file\n')
subprocess.run(['touch dummy_file'], shell=True)

### Show list of files that could not be validated ###

if len(manual) > 0:
	print('\nThese files could not be validated and must be downloaded manually. Please refer to the LENS documentation.')
	for item in manual:
		print(item)
else:
	print('All general references downloaded successfully!')
